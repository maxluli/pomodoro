import { createApp } from 'vue'
import App from './App.vue'
import '../fontawesome-free-5.15.4-web/css/all.css';
import axios from 'axios'
import VueAxios from 'vue-axios'

const app = createApp(App);

app.use(VueAxios, axios)

app.mount("#app");



//createApp(App).mount('#app')
